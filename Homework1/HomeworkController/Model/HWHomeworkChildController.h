//
//  HWHomeworkChildController.h
//  Homework1
//
//  Created by Vladislav Grigoriev on 03/10/16.
//  Copyright © 2016 com.inostudio.ios.courses. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol HWHomeworkChildController <NSObject>

@property (nonatomic, weak) id delegate;

@end
