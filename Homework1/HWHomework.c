//changed!!
//  HWHomework.c
//  Homework1
//
//  Created by Vladislav Grigoriev on 21/09/16.
//  Copyright © 2016 com.inostudio.ios.courses. All rights reserved.
//

#include "HWHomework.h"
#include "stdlib.h"
#include <math.h>
#include <string.h>

/*
 * Задание 1. (1 балл)
 *
 * Необходмо реализовать метод, который создаст матрицу и заполнит её случайными числами от -100 до 100.
 *
 * Параметры:
 *      rows - количество строк в матрице;
 *      columns - количество столбцов в матрице;
 *
 * Возвращаемое значение:
 *      Матрица размером [rows, columns];
 */
    long** createMatrix(const long rows, const long columns) {
        
        long **mas = (long**)malloc(rows*sizeof(long*));
        
        for (int i = 0; i < rows; i++){
            mas[i] = (long*)malloc(columns*sizeof(long));
            for (int j = 0; j < columns; j++)
                mas[i][j] = rand() %201 - 100;
        }
        return mas;
}

/*
 * Задание 2.
 *
 * Необходимо реализовать метод, котрый преобразует входную строку, состоящую из чисел, в строку состоящую из числительных.
 * Задание имеет два уровня сложности: обычный и высокий.
 *
 * Параметры:
 *      numberString - входная строка (например: "1", "123.456");
 *
 * Возвращаемое значение:
 *      Обычный уровень сложности (1 балл):
 *             Строка, содержащая в себе одно числительное или сообщение о невозможности перевода;
 *             Например: "один", "Невозможно преобразовать";
 *
 *      Высокий уровень сложности (2 балла):
 *              Строка, содержащая себе числительные для всех чисел или точку;
 *              Например: "один", "один два три точка четыре пять шесть";
 *
 *
 */
char* stringForNumberString(const char *numberString) {
    
    char *full_string;
    full_string = (char*) malloc (6*strlen(numberString));
    
    for (int i = 0; i < strlen(numberString); i++)
    {
        switch (numberString[i]) {
            case '1':
                strcat(full_string, "One ");
                break;
            case '2':
                strcat(full_string, "Two ");
                break;
            case '3':
                strcat(full_string, "Three ");
                break;
            case '4':
                strcat(full_string, "Four ");
                break;
            case '5':
                strcat(full_string, "Five ");
                break;
            case '6':
                strcat(full_string, "Six ");
                break;
            case '7':
                strcat(full_string, "Seven ");
                break;
            case '8':
                strcat(full_string, "Eight ");
                break;
            case '9':
                strcat(full_string, "Nine ");
                break;
            case '.':
                strcat(full_string, "Point ");
                break;
                
            default:
                strcat(full_string, "Error ");
                break;
        }
    }
    
    return full_string;
}


/*
 * Задание 3.
 *
 * Необходимо реализовать метод, который возвращает координаты точки в зависимости от времени с начала анимации.
 * Задание имеет два уровня сложности: обычный и высокий.
 *
 * Параметры:
 *      time - время с начала анимации в секундах (например: 0 или 1.234);
 *      canvasSize - длина стороны квадрата, на котором происходит рисование (например: 386);
 *
 * Возвращаемое значение:
 *      Структура HWPoint, содержащая x и y координаты точки;
 *
 * Особенность: 
 *      Начало координат находится в левом верхнем углу.
 *
 * Обычный уровень сложности (2 балла):
 *      Анимация, которая должна получиться представлена в файле Default_mode.mov.
 *
 * Высокий уровень сложности (3 балла):
 *      Анимация, которая должна получиться представлена в файле Hard_mode.mov.
 *
 * PS: Не обязательно, чтобы ваша анимация один в один совпадала с примером. 
 * PPS: Можно реализовать свою анимацию, уровень сложности которой больше или равен анимациям в задании.
 */

double t = 0; double y = 382/2;
HWPoint pointForAnimationTime(const double time, const double canvasSize) {
    
    double x = 30*(time - t);
    
   if (x > canvasSize/3 && x < canvasSize/2) y -= 5;
    else if (x > canvasSize/2 && x < (canvasSize*2)/3) y += 5;
    else y = 382/2;
    
    if (x > canvasSize) { x = 0; t = time;}
    
    return (HWPoint){x, y};
}





